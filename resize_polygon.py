from matplotlib import pyplot as plt
from numpy import arange
from shapely.geometry import Polygon, LineString
from shapely.affinity import scale

from read_polygons import read_polygons_from_csv
def resize_polygon(polygon, distance):
    # Get the exterior ring of the polygon
    exterior = polygon.exterior


    current_vertex = exterior.coords[-2]
    next_vertex = exterior.coords[0]
    next_next_vertex = exterior.coords[1]
    original_edge = LineString([current_vertex, next_vertex])
    next_original_edge = LineString([ next_vertex,next_next_vertex])
    normal_vector:LineString = original_edge.parallel_offset(distance,side="left")
    next_normal_vector:LineString = next_original_edge.parallel_offset(distance,side="left")
    lineEq=line_equation(normal_vector)
    next_lineEq=line_equation(next_normal_vector)
     # Create a list to store the enlarged coordinates
    enlarged_coords = []
    l=intersection_point(lineEq,next_lineEq)
    # Iterate through each pair of consecutive vertices in the exterior ring
    for i in range(len(exterior.coords) - 2):
        # Get the current and next vertices
        current_vertex = exterior.coords[i]
        next_vertex = exterior.coords[(i + 1)]
        next_next_vertex = exterior.coords[(i + 2)]
        # Create a LineString from the current and next vertices
        original_edge = LineString([current_vertex, next_vertex])
        next_original_edge = LineString([ next_vertex,next_next_vertex])

        normal_vector:LineString = original_edge.parallel_offset(distance,side="left")
        next_normal_vector:LineString = next_original_edge.parallel_offset(distance,side="left")

        lineEq=line_equation(normal_vector)
        next_lineEq=line_equation(next_normal_vector)

        enlarged_coords.append(intersection_point(lineEq,next_lineEq))

    enlarged_coords.append(l)
    enlarged_coords.append(enlarged_coords[0])

    enlarged_polygon = Polygon(enlarged_coords)

    return enlarged_polygon


def line_equation(line):
    """
    Given a Shapely LineString, returns its line equation in the form of y = mx + b.

    Parameters:
    - line: Shapely LineString

    Returns:
    - tuple (m, b): Slope (m) and y-intercept (b) of the line equation.
                   If the line is vertical, returns (None, x_intercept).
    """
    if not isinstance(line, LineString):
        raise ValueError("Input must be a Shapely LineString.")

    # Extract coordinates of the LineString
    coords = line.xy

    # Unpack coordinates
    x_coords, y_coords = coords

    # Check if the line is vertical
    if x_coords[1] - x_coords[0] == 0:
        # Vertical line, slope is undefined
        x_intercept = x_coords[0]
        return None, x_intercept

    # Calculate the slope (m)
    m = (y_coords[1] - y_coords[0]) / (x_coords[1] - x_coords[0])

    # Calculate the y-intercept (b) using the equation y = mx + b
    b = y_coords[0] - m * x_coords[0]

    return m, b

def intersection_point(line1, line2):
    """
    Given two line equations in the form y = mx + b or x = c, returns their intersection point (x, y).

    Parameters:
    - line1: tuple (m1, b1) representing the first line equation, or (None, x_intercept1) for a vertical line
    - line2: tuple (m2, b2) representing the second line equation, or (None, x_intercept2) for a vertical line

    Returns:
    - tuple (x, y): Intersection point coordinates.
    """
    m1, b1 = line1
    m2, b2 = line2

    # Check if either line is vertical
    if m1 is None:
        x = b1
        y = m2 * x + b2
    elif m2 is None:
        x = b2
        y = m1 * x + b1
    else:
        # Check if the lines are parallel
        if m1 == m2:
            raise ValueError("Lines are parallel and do not intersect.")

        # Calculate the intersection point
        x = (b2 - b1) / (m1 - m2)
        y = m1 * x + b1

    return x, y


if __name__=="__main__":
    r=read_polygons_from_csv("random_polygons.csv")
    original_polygon = r[4][1]
    enlarged_polygon = resize_polygon(original_polygon, 3)
    # Plotting the polygons
    fig, ax = plt.subplots()
    x, y = original_polygon.exterior.xy
    ax.plot(x, y, label='Original Polygon')

    x, y = enlarged_polygon.exterior.xy
    ax.plot(x, y, label='Enlarged Polygon')

    ax.legend()
    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')
    plt.autoscale(enable=True)
    plt.title('Original and Enlarged Polygons')
    plt.show()
