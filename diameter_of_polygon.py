import math
from shapely import Polygon
def distance(point1, point2):
    return math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)

def calculate_convex_polygon_diameter_optimized(polygon:Polygon):
    vertices = list(polygon.exterior.coords)
    def rotate_calipers(polygon):
        max_distance = 0
        j = 1

        for i in range(len(polygon)):
            while distance(polygon[i], polygon[(j + 1) % len(polygon)]) > distance(polygon[i], polygon[j]):
                j = (j + 1) % len(polygon)

            max_distance = max(max_distance, distance(polygon[i], polygon[j]))

        return max_distance

    # Sort vertices in counterclockwise order (required for the rotating calipers method)
    centroid = [sum(x[0] for x in vertices) / len(vertices), sum(x[1] for x in vertices) / len(vertices)]
    sorted_vertices = sorted(vertices, key=lambda x: math.atan2(x[1] - centroid[1], x[0] - centroid[0]))

    return rotate_calipers(sorted_vertices)
