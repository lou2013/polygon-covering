import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import Point
def plot_polygons(polygons, title="Polygons Plot"):
    plt.figure()
    for polygon in polygons:
        x, y = polygon.exterior.xy
        plt.plot(x, y, color='blue')
        plt.fill(x, y, alpha=0.3, color='blue')

    plt.title(title)
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.xticks(np.arange(10))
    plt.yticks(np.arange(10))
    plt.grid(True)
    plt.show()

def plot_polygon_and_circles(polygon, circle_centers,radius):
    # Extract coordinates from the polygon
    x, y = polygon.exterior.xy
    
    # Plot polygon
    plt.plot(x, y, color='blue', alpha=0.7, linewidth=2, solid_capstyle='round', zorder=2)
    
    # Plot circles and their centers
    for center in circle_centers:
        circle = Point(center).buffer(radius)  # Creating a circle with radius 0.5 around the center
        
        # Plot circle
        x, y = circle.exterior.xy
        plt.plot(x, y, color='red', alpha=0.7, linewidth=2, solid_capstyle='round', zorder=3)
        
        # Plot center
        plt.scatter(center[0], center[1], color='black', marker='o', zorder=4)
    
    # Set aspect ratio
    plt.gca().set_aspect('equal', adjustable='box')
    
    # Show plot
    plt.grid(True)
    plt.show()
