import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np

# Hexagon parameters
hex_size = 1
hex_aspect_ratio = 3**0.5 / 2

# Polygon vertices
polygon_vertices = [(0, 0), (1, 0), (1.5, 0.5), (1, 1), (0, 1), (-0.5, 0.5)]

# Create hexagonal grid
hexagons = [
    patches.RegularPolygon((i, j * hex_aspect_ratio), numVertices=6, radius=hex_size, orientation=np.pi/6)
    for i in range(5)
    for j in range(5)
]

# Create figure and axis
fig, ax = plt.subplots()

# Add hexagons to the plot
for hexagon in hexagons:
    ax.add_patch(hexagon)

# Add the polygon to the plot
polygon = patches.Polygon(polygon_vertices, closed=True, edgecolor='red', facecolor='none')
ax.add_patch(polygon)

# Set axis limits
ax.set_xlim(-2, 7)
ax.set_ylim(-2, 7 * hex_aspect_ratio)

# Display the plot
plt.show()
