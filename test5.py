import threading
import time
from typing import Tuple, List
from shapely.geometry import Polygon, Point
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from numpy import arange
from itertools import product
from delta import hexagonal_lattice_points
from diameter_of_polygon import calculate_convex_polygon_diameter_optimized
from plot_polygons import plot_polygon_and_circles
from polygon_distance_to_point import distance_to_nearest_point
from read_polygons import read_polygons_from_csv
from reset_centers import reset_centers
from reset_polygon import reset_polygon
from resize_polygon import resize_polygon
from rotate import rotate_shapely_polygon
from scale import scale_polygon

from translate import translate_polygon
filename = 'random_polygons.csv'
polygons_read = read_polygons_from_csv(filename)


def remove_duplicate_centers(hexagons):
    unique_centers = set()
    unique_hexagons = []
    for hexagon in hexagons:
        center_tuple = tuple(hexagon.center)
        if center_tuple not in unique_centers:
            unique_centers.add(center_tuple)
            unique_hexagons.append(hexagon)
    return unique_hexagons


def process_iteration(args):
    polygon, degree, _ = args
    rotated_polygon = rotate_shapely_polygon(polygon, degree)
    bigger_rotated_polygon = resize_polygon(rotated_polygon, 2)
    checking_bigger_rotated_polygon = resize_polygon(rotated_polygon, 4)
    biggerDiameter = calculate_convex_polygon_diameter_optimized(
        bigger_rotated_polygon)
    _, _, biggerHexagons = hexagonal_lattice_points(biggerDiameter)
    smaller_rotated_polygon = resize_polygon(rotated_polygon, -2)
    smallerDiameter = calculate_convex_polygon_diameter_optimized(
        smaller_rotated_polygon)
    _, _, smallerHexagons = hexagonal_lattice_points(smallerDiameter)
    kkk = calculate_convex_polygon_diameter_optimized(
        checking_bigger_rotated_polygon)
    _, _, phx = hexagonal_lattice_points(kkk)
    # d = calculate_convex_polygon_diameter_optimized(
    #     polygon)
    # _, _, hexs = hexagonal_lattice_points(d)
    returning_hexagon = [hexagon for hexagon in smallerHexagons if hexagon.isWithinPolygon(
        smaller_rotated_polygon) or hexagon.doesIntersectPolygon(smaller_rotated_polygon)]
    translation_hexagons = [hexagon for hexagon in biggerHexagons if (hexagon.isWithinPolygon(bigger_rotated_polygon) or hexagon.doesIntersectPolygon(
        bigger_rotated_polygon)) and not (hexagon.isWithinPolygon(smaller_rotated_polygon) or hexagon.doesIntersectPolygon(smaller_rotated_polygon))]
    checking_hexagons = [hexagon for hexagon in phx if (hexagon.isWithinPolygon(checking_bigger_rotated_polygon) or hexagon.doesIntersectPolygon(
        checking_bigger_rotated_polygon)) and not (hexagon.isWithinPolygon(smaller_rotated_polygon) or hexagon.doesIntersectPolygon(smaller_rotated_polygon))]

    translations = []
    for hexagon in translation_hexagons:
        for point in hexagon.boundary_points:
            dx, dy, _ = distance_to_nearest_point(
                rotated_polygon, Point((point[0], point[1])))
            if (dx < 1.5 and dy < 1.5):
                translations.append((dx, dy))
    min_result = []
    print(len(checking_hexagons)*len(translations))
    for dx, dy in translations:
        translated_polygon = translate_polygon(rotated_polygon, dx, dy)
        iter_result = [hexagon for hexagon in
                       checking_hexagons if
                       hexagon.isWithinPolygon(translated_polygon) or
                       hexagon.doesIntersectPolygon(translated_polygon)]
        if not min_result or len(min_result) >= len(iter_result):
            min_result = iter_result
            min_translation = (dx, dy)
    returning_hexagon.extend(min_result)
    # returning_hexagon = remove_duplicate_centers(returning_hexagon)
    return returning_hexagon, min_translation[0], min_translation[1], degree


def process_polygon_worker(polygonObject: Tuple[str, Polygon], radius: float, translation_precision: float):
    print(polygonObject[0])
    centered_polygon = translate_polygon(
        polygonObject[1], -polygonObject[1].centroid.x, -polygonObject[1].centroid.y)
    polygon = scale_polygon(centered_polygon, 1 / radius)
    min_result = []

    degrees = arange(0, 31, 1)
    start_time = time.time()
    with ProcessPoolExecutor() as executor:
        iteration_args = [(polygon, degree, translation_precision)
                          for degree in degrees]
        k = min(executor.map(process_iteration, iteration_args),
                key=lambda x: len(x[0]))
    plot_polygon_and_circles(reset_polygon(polygon, degree=k[3], dx=k[1], dy=k[2], radius=radius), reset_centers(
        hexagons=k[0], radius=radius), radius)
    print({"points": list(map(lambda x: x.center, k[0])), "numbers": len(
        k[0]), "dx": k[1], "dy": k[2], "deg": k[3]})
    end_time = time.time()

    # Calculate the elapsed time
    elapsed_time = end_time - start_time
    print(elapsed_time)
    return (polygonObject[0], min_result)


def process_polygon(polygonObjectList: List[Tuple[str, Polygon]], radius: float, translation_precision: float):
    with ThreadPoolExecutor() as executor:
        results = list(executor.map(process_polygon_worker, polygonObjectList,
                                    [radius for _ in range(
                                        len(polygonObjectList))],
                                    [translation_precision for _ in range(len(polygonObjectList))]))

    return results


if __name__ == "__main__":
    process_polygon(polygons_read[1:2], 3, 0.1)
