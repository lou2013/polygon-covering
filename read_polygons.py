import csv
from os import error
from shapely.geometry import Polygon
from shapely import from_wkt
from typing import List, Tuple
def read_polygons_from_csv(filename)->List[Tuple[int,Polygon]]:
    polygons = []

    with open(filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        header = next(csvreader)  # Skip the header row

        for row in csvreader:
            polygon_id, polygon_wkt = row
            polygon :Polygon= from_wkt(polygon_wkt)
            polygons.append((polygon_id,polygon))

    return polygons

if __name__ == "__main__":
    filename = 'random_polygons.csv'  # Change this to your CSV file name
    polygons_read = read_polygons_from_csv(filename)
    print(list(map(lambda x:x[1].area,polygons_read)))
