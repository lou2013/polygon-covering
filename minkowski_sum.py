from shapely.geometry import Polygon
from typing import List, Tuple
def minkowski_sum(polygon1:Polygon, polygon2:Polygon)->Polygon:
    """
    Calculate the Minkowski sum of two polygons.

    Parameters:
    - polygon1: Shapely Polygon object representing the first polygon.
    - polygon2: Shapely Polygon object representing the second polygon.

    Returns:
    - Shapely Polygon object representing the Minkowski sum of the input polygons.
    """

    # Ensure the input polygons are valid Shapely Polygon objects
    if not isinstance(polygon1, Polygon) or not isinstance(polygon2, Polygon):
        raise ValueError("Input must be Shapely Polygon objects.")

    # Get the vertices of the input polygons
    points1:List[Tuple[int,int]] = list(polygon1.exterior.coords) 
    points2:List[Tuple[int,int]] = list(polygon2.exterior.coords)

    # Calculate the Minkowski sum by adding all combinations of vertices
    result_vertices = set(tuple(x1+x2 for x1,x2 in zip(p1,p2)) for p1 in points1 for p2 in points2)

    # Create a new polygon from the Minkowski sum vertices
    print(result_vertices)
    result_polygon = Polygon(list(result_vertices))

    return result_polygon


polygon1 = Polygon([(0, 0), (0, 1), (1, 1), (1, 0)])
polygon2 = Polygon([(0, 0), (0, 1), (1, 1)])

result = minkowski_sum(polygon1, polygon2)
print(result)
