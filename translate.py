from shapely.affinity import translate
from shapely.geometry import Polygon
import matplotlib.pyplot as plt
def translate_polygon(polygon, dx, dy):
    """
    Translates a Shapely polygon by the specified amounts in the x and y directions.

    Parameters:
    - polygon: Shapely Polygon
    - dx: Translation along the x-axis
    - dy: Translation along the y-axis

    Returns:
    - Translated Shapely Polygon
    """
    translated_polygon = translate(polygon, xoff=dx, yoff=dy)
    return translated_polygon


if __name__=="__main__":
    # Example usage:
    original_polygon = Polygon([(0, 0), (1, 0), (1, 1), (0, 1)])

    # Translate the polygon by dx = 2, dy = 3
    translated_polygon = translate_polygon(original_polygon, 2, 3)

    # Plot the original and translated polygons


    x, y = original_polygon.exterior.xy
    plt.plot(x, y, label='Original Polygon')

    x, y = translated_polygon.exterior.xy
    plt.plot(x, y, label='Translated Polygon')

    plt.legend()
    plt.show()
