import csv
import random
from shapely.geometry import Polygon

def generate_random_polygon(min_area, max_area):
    # Generate a random number for the number of vertices (between 3 and 10)
    num_vertices = random.randint(3, 50)

    # Generate random points within a unit square
    points = [(random.random(), random.random()) for _ in range(num_vertices)]

    # Create a convex hull from the random points
    hull = Polygon(points).convex_hull

    # Calculate the area of the convex hull
    area = hull.area
    
    target_area = random.randrange(min_area, max_area)
    # Rescale the convex hull to have the desired area
    scaling_factor = (target_area / area) ** 0.5
    scaled_coordinates = [(x * scaling_factor, y * scaling_factor) for x, y in hull.exterior.coords]
    scaled_hull = Polygon(scaled_coordinates)

    return scaled_hull

def save_polygons_to_csv(polygons, filename):
    with open(filename, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(['Polygon_ID', 'Polygon'])
        
        for i, polygon in enumerate(polygons):
            csvwriter.writerow([i+1, polygon.wkt])

if __name__ == "__main__":
    min_area = 400
    max_area = 4000
    num_polygons = 200  

    polygons = [generate_random_polygon(min_area, max_area) for _ in range(num_polygons)]

    save_polygons_to_csv(polygons, 'random_polygons.csv')
