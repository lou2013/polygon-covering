from shapely import Point, Polygon
from typing import Tuple
import numpy as np
from scipy.optimize import linprog

def support_function(omega, theta):
    def line_equation(p):
        return np.cos(theta) * p[0] + np.sin(theta) * p[1]


    # Set up linear programming problem
    c = [0, 0]
    A = np.vstack((-np.array([np.cos(theta), np.sin(theta)]), np.array([np.cos(theta), np.sin(theta)])))
    b = np.array([-line_equation([0, 0]), line_equation([1, 1])])  # Just a feasible bounding box

    # Solve linear programming problem to find supremum
    result = linprog(c, A_ub=A, b_ub=b)
    supremum = -result.fun  # Minimization problem, so negate the result

    return supremum

# Example usage
theta_values = np.linspace(0, 2 * np.pi, 100)
omega = # Your domain Ω (e.g., a polygon defined by its vertices)

support_values = [support_function(omega, theta) for theta in theta_values]




def domain_omega(p:Tuple[int,int], polygon:Polygon):
    """
    Implementation of the domain Ω for a polygon.
    Parameters:
    p (tuple): A point in 2D space represented as a tuple (x, y).
    polygon (Polygon): The polygon defining the domain Ω.
    Returns:
    bool: True if the point is inside Ω, False otherwise.
    """
    point = Point(p[0], p[1])
    return point.within(polygon)
