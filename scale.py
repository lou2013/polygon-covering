from shapely.geometry import Polygon
from shapely.affinity import scale
import matplotlib.pyplot as plt
def scale_polygon(original_polygon:Polygon, scaling_factor:float):
    """
    Get a congruent polygon by scaling the original polygon.

    Parameters:
    - original_polygon (shapely.geometry.Polygon): The original polygon.
    - scaling_factor (float): The factor by which to scale the polygon.

    Returns:
    - shapely.geometry.Polygon: The congruent polygon.
    """
    if not isinstance(original_polygon, Polygon):
        raise ValueError("Input must be a Shapely Polygon.")

    if not isinstance(scaling_factor, (int, float)):
        raise ValueError("Scaling factor must be a numerical value.")

    if scaling_factor <= 0:
        raise ValueError("Scaling factor must be greater than zero.")

    # Use the affinity module to scale the original polygon
    coordinates = list(original_polygon.exterior.coords)

    # Scale each coordinate by the factor
    scaled_coordinates = [(scaling_factor * x, scaling_factor * y) for x, y in coordinates]


    congruent_polygon=Polygon(scaled_coordinates)

    return congruent_polygon


def plot_polygons(original_polygon,congruent_polygon):
    fig, ax = plt.subplots()


    # Plot original polygon in blue
    x, y = original_polygon.exterior.xy
    ax.plot(x, y, color='blue', label='Original')

    # Plot congruenced polygon in red
    x, y = congruent_polygon.exterior.xy
    ax.plot(x, y, color='red', label='Congruenced')

    ax.legend()
    ax.set_aspect('equal', adjustable='box')  # Equal aspect ratio for x and y axes
    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')
    plt.title('Original and Congruenced Polygons')
    plt.show()


if __name__=="__main__":
    # Example usage:
    original_polygon = Polygon([(0, 0), (1, 0), (1, 1), (0, 1)])
    scaling_factor = 1/4

    congruent_polygon = scale_polygon(original_polygon, scaling_factor)
    print("Original Polygon:", original_polygon)
    print("Congruent Polygon:", congruent_polygon)
    plot_polygons(original_polygon,congruent_polygon)