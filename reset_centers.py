

from rotate import rotate_shapely_polygon
from scale import scale_polygon
from translate import translate_polygon
from shapely import Polygon
from delta import Hexagon
from typing import List, Tuple 
def reset_centers(hexagons:List[Hexagon],radius:float):
    return list(map(lambda x : (x.center[0]*radius,x.center[1]*radius),hexagons))