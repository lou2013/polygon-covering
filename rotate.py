from shapely.geometry import Polygon
from shapely.affinity import rotate
import matplotlib.pyplot as plt

def rotate_shapely_polygon(polygon, angle_degrees):
    rotated_polygon = rotate(polygon, angle_degrees, origin='center', use_radians=False)
    return rotated_polygon

def plot_shapely_polygon(original_polygon, rotated_polygon):
    x, y = original_polygon.exterior.xy
    rotated_x, rotated_y = rotated_polygon.exterior.xy

    plt.figure(figsize=(8, 8))
    plt.plot(x, y, label='Original Polygon', marker='o')
    plt.plot(rotated_x, rotated_y, label='Rotated Polygon', marker='o')
    plt.legend()
    plt.grid(True)
    plt.title('Polygon Rotation using Shapely')
    plt.show()

if __name__=="__main__":
    polygon_coordinates = [(0, 0), (1, 0), (1, 1), (0, 1)] 
    polygon = Polygon(polygon_coordinates)
    rotation_angle = 45  

    rotated_polygon = rotate_shapely_polygon(polygon, rotation_angle)
    plot_shapely_polygon(polygon, rotated_polygon)
