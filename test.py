import threading
from typing import Tuple 
from matplotlib.patches import Polygon
from numpy import arange
from delta import hexagonal_lattice_points
from diameter_of_polygon import calculate_convex_polygon_diameter_optimized
from plot_polygons import plot_polygons
from read_polygons import read_polygons_from_csv
import concurrent.futures
from rotate import rotate_shapely_polygon
from scale import scale_polygon
import os
from itertools import product
from translate import translate_polygon


filename = 'random_polygons.csv'
polygons_read = read_polygons_from_csv(filename)


def process_polygon(polygonObject: Tuple[str, Polygon], radius: float, translation_precision: float):
    print(polygonObject[0])
    thread_id = threading.get_ident()
    print(f"Thread/Process ID: {thread_id}, Processing polygon {polygonObject[0]}")
    
    # Scale the polygon outside the main loop
    polygon = scale_polygon(polygonObject[1], 1 / radius)
    diameter = calculate_convex_polygon_diameter_optimized(polygon)
    _, _, hexagons = hexagonal_lattice_points(diameter)
    print(len(hexagons))
    min_result = []
    min_translation = (0, 0)
    
    # Create a list of all possible translation combinations
    translations = product(arange(-1.5, 1.5, translation_precision), repeat=2)
    
    for dx, dy in translations:
        translated_polygon = translate_polygon(polygon, dx, dy)
        
        for degree in range(0, 60, 1):
            # Rotate the polygon
            print(f"degree:{degree},dx:{dx},dy:{dy}")
            rotated_polygon = rotate_shapely_polygon(translated_polygon, degree)
            result_hexagons = [hexagon for hexagon in hexagons if hexagon.isWithinPolygon(rotated_polygon) or hexagon.doesIntersectPolygon(rotated_polygon)]
            
            if not min_result or len(min_result) > len(result_hexagons):
                min_result = result_hexagons
                min_degree = degree
                min_translation = (dx, dy)
                
    print(
        f"Processed polygon {polygonObject[0]} with degree {min_degree}, "
        f"{len(result_hexagons)} times, and translation {min_translation}"
    )
    
    return map(lambda x: (polygonObject[0], x.center), min_result)

with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    results = list(executor.map(process_polygon, polygons_read[0:1], [3 for _ in range(len(polygons_read[0:1]))],[0.1 for _ in range(len(polygons_read[0:1]))]))
    
# for polygon in polygons_read[0:1]:
    # process_polygon(polygon,20)
