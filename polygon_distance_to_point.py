import matplotlib.pyplot as plt
from shapely.geometry import Point, Polygon

from translate import translate_polygon

def distance_to_nearest_point(polygon, point):
    # Calculate the nearest point on the polygon to the given point
    nearest_point = polygon.exterior.interpolate(polygon.exterior.project(point))

    # Calculate the differences in x and y coordinates
    dx = point.x - nearest_point.x
    dy = point.y - nearest_point.y

    return dx, dy, nearest_point

if __name__=="__main__":
    # Example usage:
    polygon_coords = [(0, 0), (0, 2), (2, 2)]
    point_coords = (0.5, 1.75)

    polygon = Polygon(polygon_coords)
    point = Point(point_coords)

    # dx, dy, nearest_point = distance_to_nearest_point(polygon, point)
    dx, dy, nearest_point = distance_to_nearest_point(polygon, point)
    tp = translate_polygon(polygon,dx,dy)
    print(nearest_point)
    print(dx,dy)
    # Plotting
    x, y = polygon.exterior.xy
    x1,y1 = tp.exterior.xy
    plt.plot(x, y, color='blue', alpha=0.7, linewidth=2, solid_capstyle='round', zorder=2)
    plt.plot(x1, y1, color='green', alpha=0.7, linewidth=2, solid_capstyle='round', zorder=2)

    plt.plot(point.x, point.y, 'ro', label='Point', zorder=4)
    plt.plot(nearest_point.x, nearest_point.y, 'go', label='Nearest Point', zorder=3)

    plt.plot([point.x, nearest_point.x], [point.y, nearest_point.y], 'k--', label='Distance', zorder=1)

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Distance from Point to Nearest Point on Polygon')
    plt.legend()
    plt.grid(True)
    plt.axis('equal')
    plt.show()
