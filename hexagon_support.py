import numpy as np

def hexagon_support_function(phi):
    # Define the vertices of a regular hexagon centered at the origin
    vertices = np.array([[np.cos(np.pi/3 * i + phi), np.sin(np.pi/3 * i + phi)] for i in range(6)])

    # Calculate the support function for each vertex
    support_values = [np.dot([np.cos(phi), np.sin(phi)], vertex) for vertex in vertices]

    # Return the supremum value as the support function
    return np.max(support_values)

# Example usage:
angle_phi = np.pi/4  # Replace with the desired angle in radians
hexagon_support = hexagon_support_function(angle_phi)
print(f"The support function of the hexagon at angle {angle_phi} is {hexagon_support}")
