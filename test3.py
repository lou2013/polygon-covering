import threading
from typing import Tuple, List
from shapely.geometry import Polygon
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from numpy import arange
from itertools import product
from delta import hexagonal_lattice_points
from diameter_of_polygon import calculate_convex_polygon_diameter_optimized
from plot_polygons import plot_polygon_and_circles
from read_polygons import read_polygons_from_csv
from reset_centers import reset_centers
from reset_polygon import reset_polygon
from rotate import rotate_shapely_polygon
from scale import scale_polygon

from translate import translate_polygon
filename = 'random_polygons.csv'
polygons_read = read_polygons_from_csv(filename)

def process_iteration(args):
    polygon, hexagons, dx,dy, degree = args
    translated_polygon = translate_polygon(polygon, dx, dy)
    rotated_polygon = rotate_shapely_polygon(translated_polygon, degree)
    result_hexagons = [hexagon for hexagon in hexagons if hexagon.isWithinPolygon(rotated_polygon) or hexagon.doesIntersectPolygon(rotated_polygon)]
    return result_hexagons, dx, dy,degree

def process_polygon_worker(polygonObject: Tuple[str, Polygon], radius: float, translation_precision: float):
    print(polygonObject[0])
    centered_polygon = translate_polygon(polygonObject[1],-polygonObject[1].centroid.x,-polygonObject[1].centroid.y)
    polygon = scale_polygon(centered_polygon, 1 / radius)
    diameter = calculate_convex_polygon_diameter_optimized(polygon)
    _, _, hexagons = hexagonal_lattice_points(diameter)
    
    min_result = []
    min_translation = (0, 0)
    min_degree = 0

    degrees = arange(0, 31, 1)

    with ProcessPoolExecutor() as executor:
        iteration_args =  [(polygon, hexagons, dx,dy, degree) for dx,dy,degree in product(arange(-1.5, 1.6, translation_precision),arange(-1.5, 1.6, translation_precision), degrees)]
        k = min(executor.map(process_iteration, iteration_args),key=lambda x: len(x[0]))
    plot_polygon_and_circles(reset_polygon(polygon,k,radius),reset_centers(k,radius),radius)   
    print({"points":list(map(lambda x : x.center,k[0])),"numbers":len(k[0]),"dx":k[1],"dy":k[2],"deg":k[3]})           
    
    return (polygonObject[0], min_result)

def process_polygon(polygonObjectList: List[Tuple[str, Polygon]], radius: float, translation_precision: float):
    with ThreadPoolExecutor() as executor:
        results = list(executor.map(process_polygon_worker, polygonObjectList,
                                    [radius for _ in range(len(polygonObjectList))],
                                     [translation_precision for _ in range(len(polygonObjectList))]))
    
    return results
if __name__=="__main__":
    process_polygon(polygons_read[0:1],3,0.1)