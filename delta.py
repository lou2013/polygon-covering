from math import sqrt
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Polygon as PltPolygon
from shapely import Polygon as ShapelyPolygon

class Hexagon:
    def __init__(self, center, boundary_points):
        self.center = center
        self.boundary_points = boundary_points
    def toPlotPolygon(self):
        return PltPolygon(self.boundary_points,closed=True, edgecolor='g', fill=False)
    def toShapelyPolygon(self):
        return ShapelyPolygon(list(self.boundary_points))
    def isWithinPolygon(self,polygon:ShapelyPolygon):
        return self.toShapelyPolygon().within(polygon)
    def doesIntersectPolygon(self,polygon:ShapelyPolygon):
        return self.toShapelyPolygon().intersects(polygon)        
def hexagonal_lattice_points(D:float,step=1.0):
    sqrt3 = np.sqrt(3)
    sqrt3_over_2 = sqrt3 / 2

    # Generate lattice points within ∆
    lattice_center_points = []
    
    r = [*np.arange(0, D + 3,step), *-np.arange(0, D + 3,step)[1:]]
    for m in r:
        for n in r:
            x = m * np.array([sqrt3, 0])
            y = n* np.array([sqrt3_over_2, 3 / 2])
            point = x + y
            if point[0] >= -D - 3 and point[0] <= D + 3 and point[1] >= -D - 3 and point[1] <= D + 3:
                lattice_center_points.append(point)
    boundary_points= [point + (hexagon_support_function(phi) * np.cos(phi), hexagon_support_function(phi) * np.sin(phi))
                 for point in lattice_center_points
                 for phi in np.linspace((1/6) * np.pi, (11/6) * np.pi, 6)]
    hexagons = []
    for point in lattice_center_points:
        hexagon_boundary_points = [
        point + (hexagon_support_function(phi) * np.cos(phi), hexagon_support_function(phi) * np.sin(phi))
        for phi in np.linspace((1 / 6) * np.pi, (11 / 6) * np.pi, 6)
        ]
        hexagon = Hexagon(center=point, boundary_points=np.array(hexagon_boundary_points))
        hexagons.append(hexagon)
    return np.array(lattice_center_points),np.array(boundary_points),hexagons

def hexagon_support_function(phi):
    # Support function of the hexagon h9(ϕ)
    return np.cos(np.pi / 6 - phi % (2 * np.pi / 6))

def plot_hexagonal_lattice(D):
    lattice_points,boundary_points,hexagons = hexagonal_lattice_points(D,1)
    plt.scatter(lattice_points[:, 0], lattice_points[:, 1], color='blue', label=r'$\Lambda_h$ in $\Delta$')
    for hexagon in hexagons:
        plt.gca().add_patch(hexagon.toPlotPolygon())
    plt.scatter(boundary_points[:, 0], boundary_points[:, 1], color='purple', label=r'$\Lambda_h$ in $\Delta$')

    # Set plot properties
    plt.title('Hexagonal Lattice Points in ∆')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend()
    plt.grid(True)
    plt.show()
if __name__=="__main__":
    # Example usage:
    D = 5  # Replace with your desired diameter
    plot_hexagonal_lattice(D)
