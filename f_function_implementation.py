import numpy as np

def calculate_rmax(sigma_0, L):
    """
    Calculate rmax based on the given assumption.

    Parameters:
    - sigma_0: Positive constant hyperparameter.
    - L: Positive constant hyperparameter.

    Returns:
    - rmax: The distance beyond which any two points are assumed to be uncorrelated.
    """

    rmax = np.sqrt(6 * L)

    return rmax
def f(x, S, sigma_0, L, sigma_noise):
    """
    Calculate the estimation error f(x) based on the given formula.

    Parameters:
    - x: Spatial coordinates at which the estimation error is calculated.
    - S: Measurement set, a 2D array representing spatial coordinates {x1, ..., xn}.
    - sigma_0: Positive constant hyperparameter.
    - L: Positive constant hyperparameter.
    - sigma_noise: Variance of the zero-mean Gaussian noise.

    Returns:
    - The estimation error f(x) at the specified spatial coordinates.
    """

    # Calculate the covariance matrix CS
    CS = np.array([[squared_exponential_covariance(xi, xj, sigma_0, L) for xj in S] for xi in S]) + sigma_noise**2 * np.eye(len(S))

    # Calculate bx,S vector
    bx_S = np.array([squared_exponential_covariance(x, xi, sigma_0, L) for xi in S])

    # Calculate the estimation error f(x)
    f_x = squared_exponential_covariance(np.zeros_like(x), np.zeros_like(x), sigma_0, L) - np.dot(bx_S.T, np.linalg.solve(CS, bx_S))

    return f_x

# Helper function for squared exponential covariance
def squared_exponential_covariance(x, y, sigma_0, L):
    distance = np.linalg.norm(x - y)
    covariance = sigma_0**2 * np.exp(-distance**2 / (2 * L**2))
    return covariance

# Example usage:
x = np.array([1.0, 2.0])  # Replace with your specific spatial coordinates
S = np.array([[3.0, 4.0], [5.0, 6.0]])  # Replace with your specific measurement set
sigma_0 = 1.0  # Replace with your specific hyperparameter value
L = 2.0  # Replace with your specific hyperparameter value
sigma_noise = 0.1  # Replace with your specific noise variance

estimation_error = f(x, S, sigma_0, L, sigma_noise)
print("Estimation error f(x):", estimation_error)